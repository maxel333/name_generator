import numpy as np
import tensorflow as tf
from keras.models import Sequential
from keras.layers import LSTM, Dense, Embedding
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.callbacks import Callback
import pandas as pd

company_names = [
    "Acme Corporation",
    "Beta Industries",
    "Omega Enterprises",
    "Nova Systems",
    "Fusion Innovations",
    "Quantum Solutions",
    "Vertex Enterprises",
    "Pinnacle Tech",
    "Nexa Ventures",
    "Zenith Labs",
    "Avalon Systems",
    "Infinite Innovations",
    "Apex Innovators",
    "Stratus Dynamics",
    "Horizon Technologies",
    "Vantage Solutions",
    "Eclipse Group",
    "Orion Enterprises",
    "Astra Corp",
    "Stellar Industries",
    "Spectrum Labs",
    "Cosmic Ventures",
    "FutureTech Solutions",
    "Nebula Innovations",
    "Constellation Corp",
    "Supreme Systems",
    "Solstice Technologies",
    "Voyager Industries",
    "Galaxy Labs",
    "Quantum Enterprises",
    "Astro Innovations",
    "InfiniTech Solutions",
    "Orbit Group",
    "Genesis Systems",
    "Starlight Ventures",
    "Aurora Labs",
    "Celestial Corp",
    "Zenith Innovators",
    "Apex Dynamics",
    "Horizon Ventures",
    "Nova Labs",
    "Pioneer Technologies",
    "Fusion Corp",
    "Nexa Innovations",
    "Cosmic Dynamics",
    "Orion Innovators",
    "Stratus Corp",
    "Astra Innovations",
    "Omega Innovators",
    "Stellar Dynamics",
    "Spectrum Innovations",
    "Voyager Innovators",
    "Solstice Ventures",
    "Supreme Innovations",
    "Avalon Innovators",
    "Nebula Dynamics",
    "Constellation Innovations",
    "Infinite Dynamics",
    "Genesis Innovations",
    "Starlight Dynamics",
    "Vantage Innovations",
    "Eclipse Innovations",
    "Quantum Innovations",
    "Pinnacle Innovations",
    "Horizon Innovations",
    "Aurora Innovations",
    "Voyager Dynamics",
    "Galaxy Innovations",
    "Zenith Dynamics",
    "InfiniTech Innovations",
    "Cosmic Innovations",
    "Orbit Innovations",
    "Supreme Ventures",
    "Solstice Innovations",
    "Astra Dynamics",
    "Nexa Dynamics",
    "Nova Dynamics",
    "Pioneer Ventures",
    "FutureTech Innovations",
    "Apex Innovations",
    "Quantum Ventures",
    "Spectrum Ventures",
    "Nebula Ventures",
    "Stellar Ventures",
    "Celestial Dynamics",
    "Constellation Ventures",
    "Genesis Ventures",
    "Starlight Ventures",
    "Horizon Dynamics",
    "Avalon Ventures",
    "Eclipse Ventures",
    "Omega Ventures",
    "Zenith Ventures",
    "Aurora Ventures",
    "InfiniTech Ventures",
    "Stratus Ventures",
    "Orion Ventures",
    "Solstice Ventures",
    "Supreme Ventures",
    "Cosmic Ventures",
    "Voyager Ventures",
    "Pinnacle Ventures",
    "Nexa Ventures",
    "Nova Ventures",
    "Apex Ventures",
    "FutureTech Ventures",
    "Quantum Dynamics",
    "Pioneer Dynamics",
    "Nebula Dynamics",
    "Vantage Dynamics",
    "Voyager Dynamics",
    "Galaxy Dynamics",
    "Aurora Dynamics",
    "Eclipse Dynamics",
    "Infinite Ventures",
    "Genesis Dynamics",
    "Starlight Dynamics",
    "Horizon Ventures",
    "Avalon Dynamics",
    "Zenith Dynamics",
    "Supreme Dynamics",
    "Solstice Dynamics",
    "Orion Dynamics",
    "Stratus Dynamics",
    "Spectrum Dynamics",
    "Stellar Ventures",
    "Astra Ventures",
    "Celestial Ventures",
    "Constellation Dynamics",
    "Cosmic Dynamics",
    "Infinite Dynamics",
    "Nebula Ventures",
    "Quantum Dynamics",
    "Omega Dynamics",
    "Pinnacle Dynamics",
    "Pioneer Ventures",
    "Nexa Ventures",
    "Nova Ventures",
    "FutureTech Dynamics",
    "Voyager Dynamics",
    "Eclipse Dynamics",
    "Aurora Ventures",
    "Galaxy Ventures",
    "InfiniTech Dynamics",
    "Apex Dynamics",
    "Horizon Dynamics",
    "Genesis Ventures",
    "Starlight Ventures",
    "Zenith Ventures",
    "Solstice Ventures",
    "Supreme Ventures",
    "Avalon Dynamics",
    "Stratus Ventures",
    "Orbit Dynamics",
    "Nebula Dynamics",
    "Constellation Ventures",
    "Cosmic Ventures",
    "Omega Ventures",
    "Astra Dynamics",
    "Stellar Ventures",
    "Pinnacle Dynamics",
    "Spectrum Ventures",
    "Voyager Ventures",
    "Pioneer Ventures",
    "Nexa Dynamics",
    "Nova Dynamics",
    "FutureTech Ventures",
    "Quantum Ventures",
    "Eclipse Ventures",
    "Aurora Dynamics",
    "Galaxy Ventures",
    "InfiniTech Ventures",
    "Apex Ventures",
    "Horizon Ventures",
    "Genesis Ventures",
    "Starlight Ventures",
    "Zenith Ventures",
    "Solstice Dynamics",
    "Supreme Dynamics",
    "Avalon Ventures",
    "Stratus Dynamics",
    "Orbit Ventures",
    "Nebula Ventures",
    "Constellation Dynamics",
    "Omega Ventures",
    "Cosmic Ventures",
    "Stellar Ventures",
    "Pinnacle Ventures",
    "Astra Ventures",
    "Spectrum Ventures",
]



# Tokenize the names at character level
tokenizer = Tokenizer(char_level=True)
tokenizer.fit_on_texts(company_names)

# Convert characters to sequences
sequences = tokenizer.texts_to_sequences(company_names)

# Create input sequences and labels
X = []
y = []
for seq in sequences:
    for i in range(1, len(seq)):
        X.append(seq[:i])
        y.append(seq[i])

# Pad sequences for consistent input shape
max_seq_length = max([len(seq) for seq in X])
X = pad_sequences(X, maxlen=max_seq_length, padding='pre')

# One-hot encode labels
y = tf.keras.utils.to_categorical(y, num_classes=len(tokenizer.word_index) + 1)

# Define the LSTM model
model = Sequential()
model.add(Embedding(len(tokenizer.word_index) + 1, 32, input_length=max_seq_length))
model.add(LSTM(64))
model.add(Dense(len(tokenizer.word_index) + 1, activation='softmax'))

# Compile the model
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

# Define a custom callback to store training progress
class TrainingProgressCallback(Callback):
    def on_train_begin(self, logs=None):
        self.epochs = []
        self.accuracies = []
        self.losses = []
        
    def on_epoch_end(self, epoch, logs=None):
        accuracy_percentage = logs.get('accuracy') * 100
        loss_percentage = logs.get('loss') * 100
        self.epochs.append(epoch + 1)
        self.accuracies.append(accuracy_percentage)
        self.losses.append(loss_percentage)

# Train the model with the progress callback
progress_callback = TrainingProgressCallback()
model.fit(X, y, epochs=200, verbose=0, callbacks=[progress_callback])



# Display training progress in a table
df = pd.DataFrame({
    'Epoch': progress_callback.epochs,
    'Accuracy (%)': progress_callback.accuracies,
    'Loss (%)': progress_callback.losses
})
print(df)

# Function to generate a new company name
def generate_name(model, tokenizer, max_seq_length, max_length=20, temperature=1.0):
    seed_text = ""
    generated_text = ""

    while len(generated_text) < max_length:
        sequence = tokenizer.texts_to_sequences([seed_text])[0]
        sequence = pad_sequences([sequence], maxlen=max_seq_length, padding='pre')

        predicted_probabilities = model.predict(sequence)[0]

        # Apply the temperature
        predicted_probabilities = np.log(predicted_probabilities) / temperature
        predicted_probabilities = np.exp(predicted_probabilities) / np.sum(np.exp(predicted_probabilities))

        # Sample from the predicted probabilities
        predicted_id = np.random.choice(range(len(predicted_probabilities)), p=predicted_probabilities)

        if predicted_id == 0:
            break

        predicted_char = tokenizer.index_word.get(predicted_id, "")

        if predicted_char == "":
            break

        generated_text += predicted_char
        seed_text += predicted_char

    return generated_text

# Generate a new company name
new_company_name = generate_name(model, tokenizer, max_seq_length)
print("Generated Company Name:", new_company_name)
