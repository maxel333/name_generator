# LSTM Name Generator

This project uses a Long Short-Term Memory (LSTM) model to generate names based on a training set of names. 

## How to Run

1. Ensure you have Python 3.x installed and the necessary packages: numpy, tensorflow, pandas, Keras.

2. Run the script with `python name_gen.py`.

The script will train the model for 100 epochs and then generate a new name. During training, the accuracy and loss of the model are displayed in a table at the end of each epoch.

## How It Works

The LSTM model is trained on sequences of characters from the training names. Each character in a name is tokenized, and sequences of these tokens are used as input to the model, with the next character in the sequence as the target output. The model then learns to predict the next character in a sequence, allowing it to generate new names.

The training process is displayed in a table, with each row representing an epoch. The table has columns for the epoch number, the model's accuracy during that epoch, and the model's loss during that epoch. This allows you to monitor the model's performance throughout the training process.

After training, the model generates a new name by starting with an empty string and repeatedly predicting the next character until it predicts an empty string. The generated name is then printed to the console.




## License

This project is licensed under the MIT License. See the LICENSE.md file for details.

